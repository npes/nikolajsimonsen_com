---
Content: Vagrant on windows
Initials: NISI
hide:
  - footer
---

# Install Vagrant on windows to configure vm's

Vagrant seems like a good choice for configuring and creating virtual machines (vm's)  
I have no experience using it, these are my first steps into using Vagrant. It's basically just a short writeup of my first steps using Vagrant on virtualbox.  
I am using windows 10.  

## Installation

1. Install Python [https://www.python.org/downloads/](https://www.python.org/downloads/)
2. Install virtualbox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
    - I had problems with a `missing python cores / win32api` error during installation, it seems that this is only needed if you are scripting with python againts virtualbox, you can safely disable the python option in the installer to make the error go away
3. download and install the latest Vagrant [https://developer.hashicorp.com/vagrant/downloads?product_intent=vagrant](https://developer.hashicorp.com/vagrant/downloads?product_intent=vagrant)

## First vm

I used this guide to create my first vm [https://developer.hashicorp.com/vagrant/tutorials/getting-started/getting-started-project-setup](https://developer.hashicorp.com/vagrant/tutorials/getting-started/getting-started-project-setup)
    
**Vagrant base boxes** are here [https://app.vagrantup.com/boxes/search](https://app.vagrantup.com/boxes/search)

## vagrant cheat sheet

- [https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4](https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4)

## Videos

- oxdf video on Vagrant use: [https://youtu.be/LNInq9J3uM0](https://youtu.be/LNInq9J3uM0)

## Vagrant vs. Ansible

Vagrant can be used to create and configure vm's, but it seems that most configuration needs to be configured using shell scripts.  
Ansible can be used in conjunction with vargrant to configure vm's after creation, this seems to be the better option?

[Ippsec](https://ippsec.rocks/) has good videos on ansible, I used this playlist that explains how to configure `parrot os` using ansible:  

- Building Parrot playlist [https://youtube.com/playlist?list=PLidcsTyj9JXJVIFqyHBHzrRYKPpZYFjM8](https://youtube.com/playlist?list=PLidcsTyj9JXJVIFqyHBHzrRYKPpZYFjM8)