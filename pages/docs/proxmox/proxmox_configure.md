---
Content: Proxmox 8 configuration
Initials: NISI
hide:
  - footer
---

# Configure no-subscription repositories and upgrade

I am using proxmox without subscription and edited 2 files through a shell spawned from proxmox gui to tell proxmox to use the non enterprise repositories that are configured by default.

Documentation [https://pve.proxmox.com/wiki/Package_Repositories](https://pve.proxmox.com/wiki/Package_Repositories)

1. Remove enterprise sources list
    ``` Console
    root@proxmox:~# cd /etc/apt/sources.list.d/
    root@proxmox:~# ls
    root@proxmox:~# rm pve-enterprise.list
    root@proxmox:~# cd ..
    ```
2. Create `sources.list` file
    ``` Console
    root@proxmox:~# touch /etc/apt/sources.list
    ```
    or
    ``` Console
    root@proxmox:~# nano sources.list
    ```

3. Add repositories to `sources.list` - the content should look like this
    ``` text
    deb http://deb.debian.org/debian bookworm main contrib
    deb http://deb.debian.org/debian bookworm-updates main contrib

    # Proxmox VE pve-no-subscription repository provided by proxmox.com,
    # NOT recommended for production use
    deb http://download.proxmox.com/debian/pve bookworm pve-no-subscription

    # security updates
    deb http://security.debian.org/debian-security bookworm-security main contrib
    ```

4. Configure ceph.list to use no-subscription repository - content shown here with `cat` command
    ``` Console
    root@proxmox:/etc/apt/sources.list.d# cat ceph.list 
    # deb https://enterprise.proxmox.com/debian/ceph-quincy bookworm enterprise
    deb http://download.proxmox.com/debian/ceph-quincy bookworm no-subscription
    ```

5. In the proxmox GUI go to *node->updates* and click update to fetch the list of available updates

6. Click upgrade to upgrade node and follow instructions in prompt  
**TIP: Hibernate any running vm's before rebooting the node (right click vm and select hibernate, repeat for all vm's)**