---
Content: Proxmox convert ovf
Initials: NISI
hide:
  - footer
---

# Converting vmware images to proxmox images

Besides proxmox I am using vmware workstation on my laptop and I would like to be able to run some of the images from vmware on proxmox.  
This should be possible according to:  
[https://pve.proxmox.com/wiki/Migration_of_servers_to_Proxmox_VE](https://pve.proxmox.com/wiki/Migration_of_servers_to_Proxmox_VE)

I tested this and had succes converting [Vulnerable Pentesting Lab Environment: 1](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) from vulnhub to proxmox.  

It was "pretty straight forward" .... considering I am still a proxmox noob, what I did was:

1. Copy the vmware [files](https://download.vulnhub.com/vple/VPLE.zip) from vulnhub, unzip them and open in vmware workstation
2. Export the virtual machine from vmware using the `file->Export to OVF ... ` option.  
**TIP - if you replace the .ovf extension with .ova during export you get a single file, this is what I usually do** 
3. Copy the `.ova` file to the `/tmp` directory on the proxmox server with `scp` using  
`scp VPLE.ova user@<proxmox ip>:/tmp`
4. Open the proxmox shell by selecting the node and pressing the `shell` button
5. Extract `VPLE.ova` to `.ovf` format using  
`tar -xvf VPLE.ova`  
resulting in the following files in `/tmp` :
    ``` Console
    root@proxmox:/tmp# ls -la
    -rw-r--r--  1   64   64 4030016512 May 13 10:10 VPLE-disk1.vmdk
    -rw-r--r--  1   64   64        173 May 13 10:07 VPLE.mf
    -rw-r--r--  1   64   64      14942 May 13 10:07 VPLE.ovf
    ```
6. From the `/tmp` directory import the virtual machine to proxmox using  
`qm importovf 200 /tmp/exported-vm.ovf local-lvm`  
which is kind of tricky/not intuitive.  
200 is the VM id that will used in proxmox, this has to be and id not already in use.  
local-lvm is the `storageID` of your proxmox VM storage. This confused me to think that I need to specify the folder for my storage in ie.  
`/var/lib/vz/template/iso/` which is wrong and gave me an error:  

    ```Console
    root@proxmox:/tmp# qm importovf 200 VPLE.ovf /var/lib/vz/template/iso/
    400 Parameter verification failed.
    storage: invalid format - storage ID '/var/lib/vz/template/iso/' contains illegal characters
    ```

    This [article](https://forum.proxmox.com/threads/cant-qm-import-400-parameter-verification-failed.97154/) helped me to use the command `pvesm status` to show available storage:  
        
    ``` Console
    root@proxmox:~# pvesm status
    Name            Type     Status           Total            Used       Available        %
    local            dir     active       236258376        20409668       205480676    8.64%
    vms          zfspool     active      1885863936        69213684      1816650252    3.67%
    ```

    the name column is the `storageID`  
    The correct choice for me would be to use `vms`, but I actually made a mistake and chose `local`.   
    I'll include the mistake in here to show how I fixed it later.  

    Finally, running the correct command imported the `.ovf` file!
        
    ``` Console
    root@proxmox:/tmp# qm importovf 200 VPLE.ovf local
    Formatting '/var/lib/vz/images/200/vm-200-disk-0.raw', fmt=raw size=21474836480 preallocation=off  
    transferred 0.0 B of 20.0 GiB (0.00%)  
    transferred 204.8 MiB of 20.0 GiB (1.00%)  
    transferred 409.6 MiB of 20.0 GiB (2.00%)  
    transferred 614.4 MiB of 20.0 GiB (3.00%)  
    transferred 819.2 MiB of 20.0 GiB (4.00%)  
    transferred 1.0 GiB of 20.0 GiB (5.00%)  
    transferred 1.2 GiB of 20.0 GiB (6.00%)  
    transferred 1.4 GiB of 20.0 GiB (7.00%)  
    transferred 1.6 GiB of 20.0 GiB (8.00%)  
    transferred 1.8 GiB of 20.0 GiB (9.00%)  
    transferred 2.0 GiB of 20.0 GiB (10.00%)  
    transferred 2.2 GiB of 20.0 GiB (11.00%) 
    ...... truncated
    transferred 20.0 GiB of 20.0 GiB (100.00%)
    ```

7. Back in proxmox my VM showed as `200 (VPLE)` and was turned off. Perfect!  
I assigned a network interface from the hardware section of the VM using `add` and selecting the bridge I am using and virtIO as model.  
Then I tried to start the VM but got I an error  
`TASK ERROR: storage 'local' does not support content-type 'images'`  
Ahhh, this is the mistake I mentioned. The solution is to select the harddisk in the VM hardware section and use `Disk Action` to move the disk to a storage that supports images, in my case, you guessed it `vms` (which is a zfs pool with 2 disks in raid mirror mode)  

    This worked and I was able to start the MV.

9. Bonus specific to the [Vulnerable Pentesting Lab Environment: 1](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) VM  
After staring the environment and logging in through the console I checked the ip with  
`hostname -I` as instructed and I only got  
`172.17.0.1` and no address from my DHCP ?? weird....
Troubleshooting the issue showed me that my physical NIC (ENS18) was down.  
I used `sudo ifconfig ens18 up` which brought the interface up, but still no ip address from DHCP.  
Fix was to renew the dhcp lease with  
`sudo dhclient -r`  
and then  
`sudo dhclient` to request a new lease.

    This worked and output was:  

    ``` Console
    administrator@VPLE:~ hostname -I
    192.168.1.102 172.17.0.1
    ```

Navigating to `192.168.1.102` from a machine on the network gave me the VPLE welcome screen.  
I haven't tested all the VPLE applications, only a few that seems to work. 

Remember to clean up your `/tmp` dir as the image files can consume a lot of space.