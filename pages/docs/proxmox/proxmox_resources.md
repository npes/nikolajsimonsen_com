---
Content: Proxmox 8 configuration
Initials: NISI
hide:
  - footer
---

# Proxmox resources

Misc. Resources i have used when learning about proxmox

## Documentation

- DOCS [https://pve.proxmox.com/pve-docs/index.html](https://pve.proxmox.com/pve-docs/index.html)
- HTML DOCS [https://pve.proxmox.com/pve-docs/pve-admin-guide.html](https://pve.proxmox.com/pve-docs/pve-admin-guide.html)

## Forums

