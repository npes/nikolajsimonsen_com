![Build Status](https://gitlab.com/npes/nikolajsimonsen_com/badges/main/pipeline.svg)

# jalokin.com (tidligere nikolajsimonsen.com)

Side til tech ting jeg laver.

Link til website: [https://jalokin.com](https://jalokin.com)

# Efter projektet er klonet

1. Lav et virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) windows with version: `py -3.11 -m venv env` windows without version: `py -m venv env`
2. Aktiver virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment) windows: `.\env\Scripts\activate`
3. Installer pip dependencies `pip install -r requirements.txt`
4. Kør siden lokalt `mkdocs serve` fra **pages** mappen

Hvis linkcheckeren skal deaktiveres lokalt så kør nedenstående kommando efter virtual environment er aktiveret:

```bash
set ENABLED_HTMLPROOFER=false       #windows
export ENABLED_HTMLPROOFER=false    #linux
```


## Dokumentation

- MKDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
- Theme [https://github.com/squidfunk/mkdocs-material](https://github.com/squidfunk/mkdocs-material)
- More on Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- Git revision plugin [https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/](https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/)
- linkchecker [https://github.com/scivision/linkchecker-markdown](https://github.com/scivision/linkchecker-markdown)
- PDF builder [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf)
- Deploy to server [https://skofgar.ch/software/2018/01/deploy-static-websites-using-gitlab-ci/](https://skofgar.ch/software/2018/01/deploy-static-websites-using-gitlab-ci/)
